
$VerbosePreference="continue"
$global:EventSource="UNKNOWN"
$global:LogName="Application"

Function Set-EventSource {
	param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string]
        $newVal,
        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [String]
        $logName="Application")

    $global:EventSource=$newVal
    $global:LogName=$logName
    Setup-EventLog -eventSrc $newVal -logName $logName

    }

function Setup-EventLog {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [String]
        $eventSrc,
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [String]
        $logName
    )
    if(![System.Diagnostics.EventLog]::SourceExists($eventSrc)){
        [System.Diagnostics.EventLog]::CreateEventSource($eventSrc, $logName)
    }
}


Function Write-ErrorLog {
    $mess = $args[0]
    Write-Error $mess
    Write-EventLog -LogName Application -Source $global:EventSource -EntryType Error -EventId 1 -Message $mess
}

Function Write-VerboseLog {
    $mess = $args[0]
    Write-Output $mess
    Write-EventLog -LogName Application -Source $global:EventSource -EntryType Information -EventId 1 -Message $mess
}

Export-ModuleMember -Function 'Write-VerboseLog'
Export-ModuleMember -Function 'Set-EventSource'
Export-ModuleMember -Function 'Write-ErrorLog'