param(
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoSecEdit=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoRegistry=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoAuditPols=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoStigPs=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoHrssPs=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [string]$Username,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$Admin=$false
    )
Import-Module .\configurator.psm1
$a = @{NoSecEdit = $NoSecEdit; 
    NoRegistry = $NoRegistry; 
    NoAuditPols = $NoAuditPols; 
    NoStigPs = $NoStigPs; 
    NoHrssPs = $NoHrssPs; 
    Username = $Username; 
    Admin = $Admin}
Hrss-Setup @a

