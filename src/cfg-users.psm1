Import-Module .\helpers.psm1
Function Fix-LocalUser {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [String]$name
    )
    $usr = Get-LocalUser -Name $name
    if(!$usr.PasswordExpires){
        Set-LocalUser -Name $usr.Name -PasswordNeverExpires $false
    }
}

Function Add-LocalUsers {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string]$Username,
        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [switch]$Admin=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [string]$AdminPrefix="rx",
        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [System.Security.SecureString]$Password=(ConvertTo-SecureString -String "password1234!@#$" -AsPlainText -Force)
    )
    Write-VerboseLog "Creating new local user $Username"
    $a = @{Name = $Username; Password = $Password; PasswordNeverExpires = $false}
    New-LocalUser @a
    Add-LocalGroupMember -Member $a.Name -Group "Users"
    if($Admin) {
        $a."Name" = "$AdminPrefix$Username"
        Write-VerboseLog "Creating new admin user $($a.Name)"
        New-LocalUser @a
        Add-LocalGroupMember -Member $a.Name -Group "Administrators"
    }
}


Export-ModuleMember -Function Add-LocalUsers
Export-ModuleMember -Function Fix-LocalUser
