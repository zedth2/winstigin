Import-Module .\helpers.psm1
#region SecEdit
Function Invoke-SecEdit {
    param (
    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [String]
    $ConfigFile,
    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [String]$LogDir
    )
    $filename = Split-Path -Path $ConfigFile -Leaf
    $logfile = "$($LogDir)\$($filename).log"
    Write-VerboseLog "SecEdit.exe /configure /cfg $ConfigFile /db $LogDir\$filename.sdb /log $logfile"
    SecEdit.exe /configure /cfg $ConfigFile /db $LogDir\$filename.sdb /log $logfile
}

Function Invoke-All-SecEdits {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string[]]$Configs,
        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [string]$LogDir="."
    )
    if(!(Test-Path -Path $LogDir)){
        Write-ErrorLog "ERROR : Can not find SecEdit log directory $($LogDir)"
    }
    foreach ($s in $Configs){
        if(Test-Path -Path $s) {
            Invoke-SecEdit -ConfigFile $s -LogDir $LogDir
        } else {
            Write-ErrorLog "ERROR : Can not find SecEdit file $($s)"
        }
    }
}

#endregion


Export-ModuleMember -Function Invoke-All-SecEdits
Export-ModuleMember -Function Invoke-SecEdit