Import-Module .\helpers.psm1
#region registry edits
Function Reset-RegKey {
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [String]$Path,
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [String]$Key,
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [String]$Value,
        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [String]$Type = "DWORD"
    )
    if(!(Test-Path $path)){
        New-Item -Path $path -Force
    }
    New-ItemProperty -Path $path -Name $key -Value $value -PropertyType $type -Force
}

Function Set-AllJsonKeys {
    param(
    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [String]$File
    )
    $rules = Get-Content $File | ConvertFrom-Json
    foreach ($i in $rules.items){
        Reset-RegKey -Path $i.path -Key $i.key -Value $i.value -Type $i.type
    }
    
}

Function Invoke-RegFiles {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string[]]$RegFiles
    )
    foreach($f in $RegFiles) {
        if(Test-Path -Path $f) {
            Set-AllJsonKeys -File $f
        } else {
            Write-ErrorLog "ERROR : Can not find Registry file $($f)"
        }
    }
}

#endregion


Export-ModuleMember -Function Invoke-RegFiles 