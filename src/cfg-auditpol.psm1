Import-Module .\helpers.psm1
Function Set-AuditPols {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [String]$Name
    )
    Write-VerboseLog "auditpol /restore /file:$Name"
    auditpol /restore /file:$Name
}

Function Invoke-All-AuditPols {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string[]]$Configs
    )
    foreach ($a in $Configs) {
        if(Test-Path -Path $a) {
            Set-AuditPols -Name $a
        } else {
            Write-ErrorLog "ERROR : Can not find Audit Pol file $($s)"
        }
    }
}

Export-ModuleMember -Function Invoke-All-AuditPols
