Import-Module .\helpers.psm1
Set-EventSource -newVal "HRSS_NIST_800_53"
Import-Module .\cfg-secedit.psm1
Import-Module .\cfg-registry.psm1
Import-Module .\cfg-auditpol.psm1
Import-Module .\cfg-pwsh.psm1
Import-Module .\cfg-users.psm1


#region Configuration
$alreadyRun = $false
$hrssHome = "$(Split-Path -Path $PSCommandPath  -Parent)\"
$logDir = "$($hrssHome)logs\"
$transLog = "$($logDir)root.log"
$configsDir = "$($hrssHome)etc\"
$secEditFileNames = @("secedit.priv.rights.inf", "secedit.reg.inf", "secedit.sys.acc.inf")
$auditPolFileNames = @("nist.auditpol.csv")
$jsonRegFiles = @("reg.keys.ps1.json")
$extraScripts = @("stig.win.cap.ps1")
$hrssConfigsScripts = @("hrss.admin.cfgs.ps1")
$hrssRegFiles = @("hrss.admin.reg.keys.ps1.json")

Function Check-Settings {
    New-Item -ItemType Directory -Path $logDir
}

Function Invoke-ConfigSetup {
    if($alreadyRun) {
        return;
    }
    Check-Settings
    $alreadyRun = $true
}
#endregion

Function ConvertTo-FullPath {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string[]]$Files,
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string]$Dir=".\\"
    )
    if($Dir -notmatch '\\$') {
        $Dir+= '\\'
    }
    return $Files | ForEach-Object -Process { "$Dir$_"}
}

Function Invoke-SecEdits {
    Invoke-All-SecEdits -Configs (ConvertTo-FullPath -Files $secEditFileNames -Dir $configsDir) -LogDir $logDir 
}

Function Invoke-Registry {
    Invoke-RegFiles -RegFiles (ConvertTo-FullPath -Files $jsonRegFiles -Dir $configsDir)
}

Function Invoke-AuditPols {
    Invoke-All-AuditPols -Configs (ConvertTo-FullPath -Files $auditPolFileNames -Dir $configsDir)
}

Function Invoke-Stig-PS {
    Invoke-All-PSScripts -Scripts (ConvertTo-FullPath -Files $extraScripts -Dir $configsDir)
}

Function Invoke-Hrss-Registry {
    Invoke-RegFiles -RegFiles (ConvertTo-FullPath -Files $hrssRegFiles -Dir $configsDir)
}

Function Invoke-Hrss-PS {
    Invoke-All-PSScripts -Scripts (ConvertTo-FullPath -Files $hrssConfigsScripts -Dir $configsDir)
}

Function Hrss-Setup {
    param(
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoSecEdit=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoRegistry=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoAuditPols=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoStigPs=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoHrssPs=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$NoHrssReg=$false,
        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [string]$Username="",
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [switch]$Admin=$false
    )
    $ErrorActionPreference="SilentlyContinue"
    Stop-Transcript | Out-Null
    $ErrorActionPreference="Continue"
    Invoke-ConfigSetup
    Start-Transcript -Path $transLog
    if(!$NoSecEdit){
        Write-VerboseLog "Invoking SecEdits"
        Invoke-SecEdits
    }
    if(!$NoRegistry){
        Write-VerboseLog "Setting Registry Keys"
        Invoke-Registry
    }
    if(!$NoAuditPols){
        Write-VerboseLog "Setting Audit Policies"
        Invoke-AuditPols
    }
    if(!$NoStigPs){
        Write-VerboseLog "Running STIG PowerShell"
        Invoke-Stig-PS
    }
    Set-EventSource -newVal "HRSS_CUSTOM_CONFIG"
    if(!$NoHrssPs){
        Write-VerboseLog "Running HRSS PowerShell"
        Invoke-Hrss-PS
    }
    if(!$NoHrssReg){
        Write-VerboseLog "Apply HRSS Registry changes"
        Invoke-Hrss-Registry
    }
    if(![string]::IsNullOrEmpty($Username)) {
        $par = @{Username = $Username; Admin = $Admin}
        Add-LocalUsers @par
    }
    Stop-Transcript
}

Export-ModuleMember -Function Hrss-Setup
