Import-Module .\helpers.psm1
Function Invoke-PSScript {
    param(
    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [String]$Script
    )
    . $Script
}

Function Invoke-All-PSScripts {
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string[]]$Scripts,
        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [switch]$DryRun=$false
    )
    foreach($s in $Scripts){
        if(Test-Path -Path $s) {
            Write-VerboseLog "Executing PowerShell script $s"
            if(!$DryRun) {
                Invoke-PSScript -Script $s
            }
        } else {
            Write-Error "Could not find extra PowerShell script $s"
        }
    }
}


Export-ModuleMember -Function Invoke-All-PSScripts