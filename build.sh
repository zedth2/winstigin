#!/bin/bash


FILES=(stig.win.cap.ps1 reg.keys.ps1.json secedit.priv.rights.inf secedit.sys.acc.inf secedit.reg.inf nist.auditpol.csv )
PACK_HOME=built/winpack
ETC_DIR=$PACK_HOME/etc
LOG_DIR=$PACK_HOME/logs

build() {
    echo mkdir -p $ETC_DIR $LOG_DIR
    mkdir -p $ETC_DIR $LOG_DIR
    echo emacs ./stig_configs/nist_fifty_three.org --batch -f org-babel-tangle --kill
    emacs ./stig_configs/nist_fifty_three.org --batch -f org-babel-tangle --kill
    echo emacs ./orgspec/extras.org --batch -f org-babel-tangle --kill
    emacs ./orgspec/extras.org --batch -f org-babel-tangle --kill
    echo mv ./orgspec/*.{json,ps1} $ETC_DIR
    mv ./orgspec/*.{json,ps1,txt} $ETC_DIR
    for f in ${!FILES[*]} ; do
	    echo mv ./stig_configs/${FILES[$f]} $ETC_DIR
    	mv ./stig_configs/${FILES[$f]} $ETC_DIR
    done
    echo cp `find ./src -maxdepth 1 -iregex '.*\.psm?1'` $PACK_HOME
    cp `find ./src -maxdepth 1 -iregex '.*\.psm?1'` $PACK_HOME
}

orgspec_build(){
    echo cp orgspec/* $PACK_HOME
    cp orgspec/* $PACK_HOME
}

fix_configs() {
    # Remove all blank lines from CSV because otherwise auditpol chokes
    csv_file=$ETC_DIR/nist.auditpol.csv
    awk 'NF' $csv_file > $csv_file.new
    mv $csv_file.new $csv_file


    # Have to remove last }, otherwise not actually json
    fix_json $ETC_DIR/reg.keys.ps1.json
    fix_json $ETC_DIR/hrss.admin.reg.keys.ps1.json
}
fix_json() {
    reg_file=$1
    echo Fixing $reg_file
    linNo=`grep -n "}," $reg_file | tail -1 | grep -P '[0-9]+' -o`
    let prev=$linNo-1
    head -$prev $reg_file > $reg_file.new
    echo \}\]\} >> $reg_file.new
    mv $reg_file.new $reg_file

}


clean() {
    rm -rf built
}

build
#orgspec_build
fix_configs
