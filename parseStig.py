#!/usr/bin/python3

from sys import argv
import xml.etree.ElementTree as ET

NAMESPACE = 'http://checklists.nist.gov/xccdf/1.1'

def _add_namespace(tag):
    return '{' + NAMESPACE + '}' + tag

class AllIDs:
    def __init__(self):
        self.groupId = ''
        self.ruleId = ''
        self.version = ''
        self.title = ''

    @classmethod
    def from_ids(cls, groupId, ruleId, version, title=''):
        '''
        Need this to enable inheritence but keep from_group_elem working
        Though I'm not sure why you'd ever actually need inheritence for this
        '''
        c = cls()
        c.groupId = groupId
        c.ruleId = ruleId
        c.version = version
        c.title = title
        return c

    @classmethod
    def from_group_elem(cls, elem):
        groupId = elem.attrib['id']
        rule = elem.find(_add_namespace('Rule'))
        ruleId = rule.attrib['id']
        version = rule.find(_add_namespace('version')).text
        title = rule.find(_add_namespace('title')).text
        return cls.from_ids(groupId, ruleId, version, title)

    def org_mode_str(self, titles=True):
        lines = '* {} {} {}\n'.format(self.groupId, self.ruleId, self.version)
        if self.title and titles:
            lines += '** {}\n'.format(self.title)
        return lines

    def __lt__(self, other):
        if not isinstance(other, AllIDs):
            raise TypeError('Not of a valid AllIDs type')
        me = int(self.groupId.split('-')[1])
        o = int(other.groupId.split('-')[1])
        return me < o

def parse(xmlFile):
    return ET.parse(xmlFile)

def findGroups(root):
    return root.findall(_add_namespace('Group'))

def find_all_ids(root):
    ids = []
    for g in findGroups(root):
        ids.append(AllIDs.from_group_elem(g))
    return ids

def find_all_groupids(root):
    for l in find_all_ids(root):
        print(l.groupId)

def print_org_mode(root, titles=True):
    ids = find_all_ids(root)
    ids.sort()
    for i in ids:
        print(i.org_mode_str(titles))
        print()


def argv_setup(argv):
    import argparse
    p = argparse.ArgumentParser(prog=argv[0])
    p.add_argument('files', type=argparse.FileType('r'), metavar='F', nargs='+')
    p.add_argument('-g', '--groupid', default=False, action='store_true')
    p.add_argument('--no-title', default=False, action='store_true')
    p.add_argument('-a', '--allid', default=True, action='store_true')
    return p.parse_args(argv[1:])


def main(argv):
    args = argv_setup(argv)
    if args.groupid:
        find_all_groupids(parse(args.files[0]).getroot())
    elif args.allid:
        ids = print_org_mode(parse(args.files[0]).getroot(), not args.no_title)

if __name__ == '__main__':
    main(argv)
